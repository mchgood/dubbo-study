package com.example.provider.dubbo04bootprovider.service;

import org.apache.dubbo.config.annotation.DubboService;
import org.example.api.UserInfo;
import org.example.api.service.UserService;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@DubboService
public class UserServiceImpl implements UserService {
    @Override
    public UserInfo getById(String id) {
        return new UserInfo().setName(id).setAge(18).setBirthday(new Date());
    }
}
