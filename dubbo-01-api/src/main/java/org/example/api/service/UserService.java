package org.example.api.service;

import org.example.api.UserInfo;

public interface UserService {

    UserInfo getById(String id);
}
