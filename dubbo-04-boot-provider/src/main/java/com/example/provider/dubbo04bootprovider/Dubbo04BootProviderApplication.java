package com.example.provider.dubbo04bootprovider;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableDubbo
@SpringBootApplication
public class Dubbo04BootProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(Dubbo04BootProviderApplication.class, args);
    }

}
