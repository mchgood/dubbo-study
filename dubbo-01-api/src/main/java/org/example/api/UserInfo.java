package org.example.api;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
public class UserInfo implements Serializable {

    private String name;

    private Integer age;


    private Date birthday;
}
