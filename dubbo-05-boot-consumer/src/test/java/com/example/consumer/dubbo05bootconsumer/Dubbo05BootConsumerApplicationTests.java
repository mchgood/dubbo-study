package com.example.consumer.dubbo05bootconsumer;

import org.apache.dubbo.config.annotation.DubboReference;
import org.example.api.UserInfo;
import org.example.api.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Dubbo05BootConsumerApplicationTests {


    @DubboReference(url = "dubbo://192.168.199.229:20880/org.example.api.service.UserService?serialization=kryo")
    private UserService userService;

    @Test
    void contextLoads() {
    }

    @Test
    public void userServiceTest(){
        UserInfo userInfo = userService.getById("123");
        System.out.println(userInfo.toString());
    }

}
