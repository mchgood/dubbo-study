package com.example.consumer.dubbo05bootconsumer;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableDubbo
@SpringBootApplication
public class Dubbo05BootConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(Dubbo05BootConsumerApplication.class, args);
    }

}
